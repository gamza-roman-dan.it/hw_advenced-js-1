"use strict"

/*
1-
прототипне наслідування це коли якась функція/метод записані у одному місці. а купа обєктів/класів
мають до нього доступ. тобто у випадку з класами, метод пишеться у батьківському класі й автоматично
він вноситься у прототип, в свою чергу дочірні/послідовні елементи можуть той метод викликати;

2-
щоб викликати батьківські методи або елементи й зберегти логіку при "наприклад" перезаписанні тогож батьківського методу чи дублюванні елементів;

*/

//------------------------------------------------------------------------------------------------------------

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    };


    get fullEmployee (){
        return `my name ${this.name}, i have ${this.age} y.o. my salary${this.salary}`;
    };
    set fullEmployee (value){
        this.salary = this.salary + value;
    };
}

class Programmer extends Employee{
    constructor(name, age, salary, lang) {
        super(name, age,);
        this.salary = salary * 3;
        this.lang = lang;
    }
}

//OR

class Programmer2 extends Employee{
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get fullEmployee () {
        return `my name ${this.name}, i have ${this.age} y.o. my salary${this.salary * 2}. and i speak ${this.lang}`;
    }
}

console.log(new Programmer("oleg", 30, 2000, "UA").fullEmployee);
console.log(new Programmer2("anton", 40, 4000, "Eng").fullEmployee);

//по моєму я правильно зрозумів, що від мене просять)))